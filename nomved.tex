%% 'xelatex''
%% 'biber'
\documentclass[12pt, a4paper]{article}

%Pacotes
%%%%% Fontes e afins
\usepackage[no-math]{fontspec}

%%%%%TEUBNER e DEPENDÊNCIA DO TEUBNER
\usepackage[greek]{babel}
\usepackage{teubner}

%Verse
\usepackage{verse}

%%%%%Polyglossia, especialmente sânscrito 
\usepackage{polyglossia}
\setmainlanguage{brazil}
\setotherlanguages{sanskrit, english}
%definições
\setmainfont[BoldFont={Gentium Basic Bold},ItalicFont={Gentium Italic}]{Gentium Plus}
\newfontfamily
\devanagarifont[Script=Devanagari, Scale=1.2]{Chandas}

%%%%% Sobre organização e formatação
\usepackage{indentfirst}
\usepackage{titlesec}
\usepackage{paralist} % Listas mais elegantes
\usepackage{fullpage} % Maior aproveitamento da página
\usepackage{setspace} \onehalfspacing 
\usepackage{multirow}
\usepackage{longtable}
\usepackage{csquotes}
\usepackage{fancyhdr} % Header fancy
\usepackage{linguex}
%%% Coisas pro fancy header:
\usepackage[top=1.2in,bottom=1in,right=1in,left=1in,headheight=35pt,headsep=1cm]{geometry}
\pagestyle{fancy}
\fancyhead{}

\usepackage{multicol}
\setlength{\columnsep}{1.5cm}
\renewcommand{\baselinestretch}{1.5}
% Configuração do rodapé
\renewcommand{\footnotesize}{\small}
\usepackage[flushmargin, bottom]{footmisc}
\addtolength{\footnotesep}{3mm} % change to 1mm

%Configuração dos títulos de seção
\titleformat*{\section}{\normalsize\bfseries}
\titleformat*{\subsection}{\normalsize\bfseries}
\titleformat*{\subsubsection}{\normalsize\bfseries}

%% Comandos para sinais indoeuropeus
\newcommand{\iode}{i̯}
\newcommand{\wal}{u̯}
\newcommand{\lar}[1]{\textit{h\textsubscript{#1}}}
\newcommand{\sayana}{Sāyaṇa }
%Macro Vocabulário
\newcommand{\entradavoc}[3]{\noindent \textbf{#1}: \textit{#2}\hspace{4pt} #3}
%Macro de citação
\newcommand{\citegram}[2]{(Whitney $\S$ #1; Macdonell $\S$ #2)}
\newcommand{\citegramnopar}[2]{Whitney $\S$ #1 e Macdonell $\S$ #2}

\newcommand{\citewhit}[1]{Whitney $\S$ #1}
%Macro skt
\newcommand{\skt}[1]{\begin{sanskrit}#1\end{sanskrit}}

%Informações Gerais
\title{Basicão de Morfologia do Védico: Nomes}
\author{Caio Borges Aguida Geraldes}

%% Informações para o fancy header
\lhead{Védico}
\chead{\leftmark}
\rhead{\today}

%Bibliografia

\usepackage[backend=biber, style=abnt, scbib, indent, sorting=nyt, giveninits, pretty, uniquename=false]{biblatex}
\addbibresource{vedico.bib}


\begin{document}

\maketitle

\section{Morfologia nominal}

\textit{\citegramnopar{261-305}{70}}

As formas nominais do védico marcam:
\begin{enumerate}
	\item \textbf{Gênero:} masculino, feminino e neutro;
	\item \textbf{Número:} singular, dual e plural;
	\item \textbf{Caso:} nominativo, acusativo, instrumental, dativo, ablativo, genitivo e locativo + vocativo.
\end{enumerate}

É comum se dividir as classes nominais em temas vocálicos e temas consonantais. Seguirei esse modelo, embora muitas classes vocálicas tenham mais semelhanças com classes consonantais.

\subsection{Gradação vocálica}

\textit{\citegramnopar{311-20}{72-3}}


Há nomes que possuem apenas um tema, nomes que possuem dois temas e nomes que possuem três temas. Os nomes com dois temas possuem uma forma \textit{forte} e uma forma \textit{fraca}, a primeira sendo utilizada no nominativo, acusativo e vocativo singular e dual e nominativo plural de nomes masculinos e nominativo, acusativo e vocativo plural dos neutros. Os nomes com três temas possuem além do \textit{forte} e \textit{fraco} um tema \textit{médio} utilizado com desinências consonantais.


\subsection{Desinências}

\textit{\citegramnopar{307-10}{71}}

Via de regra, as desinências seguirão esse modelo:
\begin{center}
\begin{longtable}{ccccccc}
    & \multicolumn{2}{c}{Singular}            & \multicolumn{2}{c}{Dual}                    & \multicolumn{2}{c}{Plural}                 \\
    & M. F.                 & N               & M. F.                & N                    & M. F.                & N                   \\
N.  & s                     & -               & \multirow{2}{*}{ā/au}  & \multirow{2}{*}{ī} & \multirow{2}{*}{as}  & \multirow{2}{*}{i}  \\
Ac. & -(a)m                 & -               &                      &                      &                      &                     \\
I.  & \multicolumn{2}{c}{ā}                   & \multicolumn{2}{c}{\multirow{3}{*}{bhyām}}  & \multicolumn{2}{c}{bhis}                   \\
D.  & \multicolumn{2}{c}{e}                   & \multicolumn{2}{c}{}                        & \multicolumn{2}{c}{\multirow{2}{*}{bhyas}} \\
Ab. & \multicolumn{2}{c}{\multirow{2}{*}{as}} & \multicolumn{2}{c}{}                        & \multicolumn{2}{c}{}                       \\
G.  & \multicolumn{2}{c}{}                    & \multicolumn{2}{c}{\multirow{2}{*}{os}}     & \multicolumn{2}{c}{ām}                      \\
L.  & \multicolumn{2}{c}{i}                   & \multicolumn{2}{c}{}                        & \multicolumn{2}{c}{su}     
\end{longtable}
\end{center}
As excessões estarão nas classes temáticas de declinação. Assim, essas desinências são as mais comuns para a 3ª ou declinação atemática, como grego e latim.

\section{Declinações vocálicas}

\subsection{Temas em -a}
\textit{\citegramnopar{326-34}{97}}

Essa classe vale pela segunda declinação do grego e latim. É a classe mais produtiva da língua.

\begin{center}
\begin{longtable}{ccccc}
    & \multicolumn{2}{c}{Singular}  & \multicolumn{2}{c}{Plural}            \\
    & M.            & N.            & M.                & N.                \\
N.  & devás         & mṛgám         & devā́s / devā́sas   & mṛgā́ / mṛgā́ṇi     \\	
Ac. & devám         & mṛgá          & devā́n             & mṛgā́ / mṛgā́ṇi     \\
I.  & devéna / devā́ & mṛgéṇa / mṛgā́ & deváis / devébhis & mṛgáis / mṛgébhis \\
D.  & devā́ya        & mṛgā́ya        & devébhyas         & mṛgébhyas         \\
Ab. & devā́t         & mṛgā́t         & devébhyas         & mṛgébhyas         \\
G.  & devásya       & mṛgásya       & devā́nām           & mṛgā́ṇām           \\
L.  & devé          & mṛgé          & devéṣu            & mṛgéṣu                     

\end{longtable}
\begin{longtable}{ccc}
    \multicolumn{3}{c}{Dual}             \\
             & M.           & N.         \\    
    N.A.     & devā́ / deváu & mṛgé       \\
    I.D.Abl. & devā́bhyām    & mṛgā́bhyām  \\
    G.L.     & deváyos      & mṛgáyos    
\end{longtable}
\end{center}

\subsection{Temas em -ā}
\textit{\citegramnopar{362-8}{97}}

Essa classe não engloba os nomes radicais terminados em \textit{ā}, que serão vistos em \ref{radicaisema}. A maior parte dos femininos fazem parte dessa declinação, tanto em nomes quanto em adjetivo. Ela equivale aos femininos de primeira declinação em grego e latim.

\begin{center}
\begin{longtable}{ccc}
    & Singular      & Plural          \\
N.  & áśvā          & áśvās / áśvāsas \\	
Ac. & áśvām         & áśvās           \\
I.  & áśvayā / áśvā & áśvābhis        \\
D.  & áśvāyai       & áśvābhyas       \\
Ab. & áśvāyās       & áśvābhyas       \\
G.  & áśvāyās       & áśvānām         \\
L.  & áśvāyām       & áśvāsu          

\end{longtable}
\begin{longtable}{cc}
	\multicolumn{2}{c}{Dual} \\
    	N.A.     & áśve         \\
    	I.D.Abl. & áśvā́bhyām    \\
    	G.L.     & áśvayos
\end{longtable}
\end{center}

\subsection{Temas em -i e -u}
\textit{\citegramnopar{335-46}{98-9}}

Esses temas costumam ser tratados em conjunto pelas semelhanças que possuem. Nelas temos nomes de todos os gêneros, havendo prevalência de femininos nos temas em \textit{-i}.
Há algumas diferenças entre os gêneros, então colocarei o paradigma de ambas as formas no masculino anotando abaixo  as particularidades de do feminino e neutro.

\begin{center}
\begin{longtable}{ccccc}
    & \multicolumn{2}{c}{Singular}      & \multicolumn{2}{c}{Plural}  \\
    & -i             & -u               & -i         & -u         \\
N.  & śúcis          & mádhus           & śúcayas    & mádhavas   \\	
Ac. & śúcim          & mádhum           & śúcīn      & mádhūn     \\
I.  & śúcyā / śúcinā & mádhvā / mádhunā & śúcibhis   & mádhubhis  \\
D.  & śúcaye         & mádhuve          & śúcibhyas  & mádhubhyas \\
Ab. & śúces          & mádhos           & śúcibhyas  & mádhubhyas  \\
G.  & śúces          & mádhos / mádhvas & śúcīnām    & mádhūnām   \\
L.  & śúcā / śúcau   & mádhavi / mádhau & śúciṣu     & mádhuṣu                     

\end{longtable}
\begin{longtable}{ccc}
    \multicolumn{3}{c}{Dual}             \\
             & -i        & -u         \\    
    N.A.     & śúcī      & mádhū       \\
    I.D.Abl. & śúcibhyām & mádhubhyām  \\
    G.L.     & śúcyos    & mádhvos    
\end{longtable}
\end{center}

\paragraph{Feminino} \begin{inparaenum}[(i)]
\item \textbf{Instrumental singular:} os temas em -i podem formar \textit{śúcyā}, \textit{śúcī } e \textit{śúci};
\item \textbf{Genitivo singular:} os temas em -u só formam \textit{mádhos};
\item \textbf{Locativo sinfular:} os temas em -u só formam \textit{mádhau};
\item \textbf{Acusativo plural:} os temas em -i formam śúcīs e os temas em -u formam \textit{mádhūs}.
\end{inparaenum}

\paragraph{Neutro:} é comum notar em neutros a intromissão de um \textit{n} entre tema e desinência. \begin{inparaenum}[(i)]
\item \textbf{Nominativo singular:} as formas neutras não tem o -s, logo \textit{śúci } e \textit{mádhu};
\item \textbf{Instrumental singular:} tanto em -i quanto em -u, apenas a forma com o \textit{n} ocorre;
\item \textbf{Dativo singular:} nos temas em -u, a forma \textit{mádhune} ocorre;
\item \textbf{Genitivo e ablativo singular:} os temas em -u apresentam duas formas: \textit{mádhos} e \textit{mádhunas};
\item \textbf{Locativo singular:} os temas em -u podem fazer locativo em \textit{mádhavi, mádhuni} e \textit{mádhau};
\item \textbf{Dual:} os temas em -u formam nom.acu. \textit{mádhvī} e G.L.\textit{mádhunos};
\item \textbf{Nominativo e acusativo plural:} Os temas em -i fazem \textit{śúcī}, \textit{śúci} e \textit{śúcīni} e os temas em -u fazem \textit{mádhū}, \textit{mádhu} e \textit{mádhūn}.
\end{inparaenum}

\subsection{Temas em -ī e -ū}
\textit{\citegramnopar{362-8}{100}}

Geralmente correspondem a femininos e declinam diferente das formas radicais \ref{radicaisemi} e \ref{radicaisemu}. Agrupo essas declinações por conveniência, mas ambas declinam de modo um pouco diferente. Note-se nas tabelas que os temas em \textit{ī} recebem \textit{s} no nominativo e possuem desinências parecidas com as dos temas em \textit{ā}. Os temas em \textit{ū} não fazem a resolução do hiato na grafia nem como os temas em \textit{ī} nem como os de \ref{radicaisemu}. Posteriormente em sânscrito, os temas em \textit{ū} serão reformados analogicamente aos temas em \textit{ī}, \textit{vide} Whitney $\S$ 364.

\begin{center}
\begin{longtable}{ccccc}
    & \multicolumn{2}{c}{Singular}  & \multicolumn{2}{c}{Plural}  \\
    & -ī      & -ū           & -ī        & -ū         \\
N.  & devī́    & tanū́s        & devī́s     & tanúas   \\	
Ac. & devī́m   & tanúam       & devī́s     & tanúas     \\
I.  & devyā́   & tanúā        & devī́bhis  & tanū́bhis  \\
D.  & devyái  & tanúe        & devī́bhyas & tanū́bhyas \\
Ab. & devyā́s  & tanúas       & devī́bhyas & tanū́bhyas  \\
G.  & devyā́s  & tanúas       & devī́nām   & tanū́nām   \\
L.  & devyā́m  & tanúi / tanū́ & devī́ṣu    & tanū́ṣu (?)                     

\end{longtable}
\begin{longtable}{ccc}
    \multicolumn{3}{c}{Dual}             \\
             & -ī        & -ū         \\    
    N.A.     & devī́      & tanúā      \\
    I.D.Abl. & devī́bhyām & tanū́bhyām  \\
    G.L.     & devyós    & tanúos    
\end{longtable}
\end{center}

\subsection{Temas em -ṛ}
\textit{\citegramnopar{369-76}{101}}

São formados por sufixos \textit{-ar} e \textit{-tar}, declinando com algumas diferenças pequenas. Esses temas tem uma gradação \textit{forte} \textit{(t)ar/(t)ār} e uma forma \textit{fraca} \textit{r/ṛ}. O nominativo de ambos apresenta queda do \textit{r} final. O acusativo plural recebe um \textit{n} no masculino e um \textit{s} no feminino e possui sempre um \textit{n} entre tema e desinência no genitivo plural\footnote{Eventualmente essa adição causa alongamento da vogal como em \textit{nṝṇā́m}.}. O genitivo singular pode ser feito por uma desinência própria \textit{ur}, ex.: \textit{nánāndur}, embora \textit{náras}.

\subsubsection{Em -ar}

Apenas oito formas: m. \textit{devṛ́} ''irmão do marido'', \textit{nṛ́} ''homem'', f. \textit{usṛ́} ''aurora'', \textit{nánāndṛ } ''irmã do marido'', \textit{svásṛ} ''irmã'', n. \textit{áhar} ''dia'', \textit{ū́dhar} ''teta de vaca'', \textit{vádhar} '''arma''.

\subsubsection{Em -tar}

Formam-se com esse sufixos: \begin{inparaenum}[(i)]
\item \textbf{Nomes de parentesco:} \textit{pitṛ́} ''pai'', \textit{bhrā́tṛ} ''irmão'', \textit{náptṛ} ''neto'', \textit{duhitṛ} ''irmã'' e \textit{mātṛ} ''mãe'';
\item \textbf{Nomes de agente:} \textit{dātṛ́} ''doador'', \textit{hótṛ} ''sacrificante''.
\end{inparaenum}
A adição do \textit{n} no acusativo e genitivo plural e do \textit{s} no acusativo plural feminino causa o alongamento do \textit{ṛ} sempre. O genitivo é feito regularmente com a desinência \textit{ur}.

\subsection{Temas em ditongo}
\textit{\citegramnopar{360-1}{102}}

Há apenas 5 temas em ditongo: \textit{rái} ''riqueza'', \textit{gó} ''boi/vaca'', \textit{dyó} ''céu'', \textit{náu} ''navio'', \textit{gláu} ''nódulo (?)''.

\subsection{Temas radicais}

\subsubsection{Em -ā}\label{radicaisema}
\textit{\citegramnopar{348-58}{97.2}}

São raros os casos e a maioria aparece em composição. Usam-se as desinências normais ao longo da declinação. Não há diferença na declinação de masculino e feminino. A vogal da raiz cai quando seguida de desinências começadas por vogal. Exemplo: \textit{jā́} ''criança'', \textit{dā́} ''doador'', \textit{ratnadhā́} ''que dá/coloca presentes, \textit{kṣā́} ''região, abóbada'', \textit{gnā́} ''mulher (divina)''.

Muitos itens dessa classe são colocados já no RV em outras classes: 1) em femininos, ex.: \textit{svadhā́} ''autodeterminação, pátria (?)'' com o intrumental \textit{svadh\textbf{áyā}}; 2) em masculinos e neutros de da declinação em -a, ex.: \textit{ratnadhá} com o instrumental plural \textit{ratnadhébhis} (RV 4.34)\footnote{Note-se que a forma com \textit{-ā} é usada no feminino nesse mesmo hino: \textit{gnā́spátnībhī ratnadhā́bhiḥ sajóṣāḥ} ''beba junto das mulheres esposas que conferem tesouros'' (RV 4.34.7).}.		

\subsubsection{Em -ī}\label{radicaisemi}
\textit{\citegramnopar{348-58}{100}}

Geralmente femininos quando substantivos e podem ter qualquer gênero quando em compostos. Alguns temas em \textit{ī} declinam igual aos temas radicais, ex.: \textit{rathī́} ''carruagem'' (m.), \textit{ahī́} ''serpente'' (m.) , \textit{lakṣmī́} ''sinal'' e \textit{dūtī́} ''mensageira''. A declinação usa as desinências normais, salvo no genitivo plural onde apenas uma vez se atesta \textit{ām}, sendo a forma normal \textit{nām}. Antes de temas vocálicos, há a dissolução da vogal em \textit{iy} quando o tema é monossilábico, mesmo em compostos. Caso o contrário, a notação será \textit{yV} mas a pronúncia é via de regra com o hiato \textit{iV} ou \textit{i\textsubscript{y}V}. Mais exemplos: \textit{dhī́} ''pensamento'',  \textit{bhī́} ''medo'', \textit{yajñanī́} ''adoração; lit. condução do sacrifício/ritual'', \textit{yajñaprī́} ''amante dos sacrifícios''\footnote{Comparem a declinação desses dois últimos.}.

\subsubsection{Em -ū}\label{radicaisemu}
\textit{\citegramnopar{348-58}{100}}

Bastante regular, também forma principalmente femininos. Segue as desinências normais. O hiato também é dissolvido na frente de vogais fazendo \textit{uvV}. Possui duas formas não radicais que declinam como ela, \textit{juhū́} ''língua, colher de ritual'', \textit{jógū́} ''que canta alto'', e formas compostas com preposições, ex.: \textit{paribhū́} ''que fica em volta, encompassa''. As formas radicais são: \textit{dū́, bhū́, brū́, syū́, srū́, sū́} e \textit{jū́}.

\section{Declinações consonantais}

\subsection{Acento}

O acento cai no tema nas formas fortes (nom.voc.acu.sg.du.masc, nom.voc.pl.masc. e nom. voc.acu.pl.neutr.) e nas desinências nas demais formas.

\subsection{Temas imutáveis}

\textit{\citegramnopar{383-419}{75-83}}

Todos terminados em consoantes. Os temas imutáveis não apresentam apofonia, embora vez ou outra certos alongamentos ocorram\footnote{Não sei, mas chutaria ser causado uma antiga apofonia entre o/e.}. No mais, suas formas estão sempre sujeitas a sandhi. Exemplos: \textit{vā́k} "palavra", \textit{pád} ''pé'', \textit{áp} ''água'', \textit{víś} ''moradia'', \textit{sáh} ''vitorioso'', \textit{bhā́s} ''luz'', \textit{yáśas} ''fama / glória'', \textit{sváśocis} ''brilhante por si só'', \textit{cákṣus} ''visão''. 

\subsection{Temas mutáveis}

\textit{\citegramnopar{420-74}{84 e 94-6}}

As formas regulares são encontradas em derivativos em \textit{t, n, s} ou \textit{c}. Tem forma \textit{forte} e \textit{fraca} os temas em \textit{-ant, -in} e \textit{yāṃs}. Tem três formas os temas em \textit{-an, vāṃs} e \textit{-añc}. As formas femininas são feitas com a adição de um \textit{-ī} à forma \textit{fraca}; ex.: \textit{sárasvatī} ''rica em pântanos (\textit{saras}), Sarasvati (rio e deusa)'', nem sempre há atestação no entanto.

\subsubsection{-ant, -at}
\textit{\citegramnopar{443-51}{85}}

Correspondem normalmente a particípios como \textit{adánt} ''comendo'' ou nomes e adjetivos possivelmente formados de antigos particípios como \textit{mahánt} ''grande'' e \textit{dánt} ''dente''. Alguns raros exemplos apresentam alongamento do \textit{a} nas formas fortes, ex.:
\ex. \textit{mahánt}: masc.acu.sg. \textit{mahā́ntam} x masc.acu.pl. \textit{mahatás}

\subsubsection{-mant, -vant x -mat, -vat}
\textit{\citegramnopar{452-6}{86}}

Adjetivos com o sentido de ''possuidor de x'', como \textit{gómant} ''possuidor de vacas'', \textit{bhágavant} ''rico em bens''. O acento segue regras de derivação e por isso permanece o tema que forma o adjetivo. Sempre possuem alongamento do \textit{a} nas formas fortes. Os vocativos são sempre feitos em \textit{mas}. 
	
	
\subsubsection{-in, -min, vin}
\textit{\citegramnopar{439-41}{87}}

Adjetivos com o mesmo sentido do anterior, ex.: \textit{hastín} ''que possui mãos'', \textit{gāthín} ''cantor, \textit{lit. aquele que possui canção}''. Confesso que não sei como essa classe é considerada mutável, pois apenas há um tema do tipo \textit{forte}, mas sigo as gramáticas consultadas. Se eu descobrir algo sobre isso, prometo avisar. O \textit{n} desaparece nos casos em que há uma desinência consonantal. No nominativo singular, temos a queda do \textit{n} e o alongamento: \textit{aśvī́} ''o possuidor de cavalos''. O nominativo e acusativo neutros singulares também tem a queda do \textit{n} sem o alongamento: \textit{hastí}.

Mais exemplos: \textit{balín} ''forte'', \textit{aśvín} ''possuidor de cavalos'', \textit{ṛgmín} ''hineante, \textit{lit. cheio de hinos}''.

\subsubsection{-yāṃs x -yas}
\textit{\citegramnopar{463-74}{88}}

Os nomes desse tipo correspondem a comparativos (gr. μείων, jônico μέζων < meg-iōn). O morfema quase sempre é conectado por um \textit{ī}. Não há atestação de duais e o plural se restringe ao nominativo, acusativo e genitivo. Não há atestação de formas femininas. Exemplos: \textit{kánīyāṃs} ''mais novo'', \textit{bhū́yāṃs} ''mais'', \textit{sányāṃs} ''mais velho''.

\subsubsection{-vāṃs x -vas/-vat x -uṣ}
\textit{\citegramnopar{458-62}{89}}

Forma do particípio perfeito ativo, ex.: \textit{cakṛvā́ṃsam} masc.acu.sg. de \textit{kṛ} ''fazer''. O acento permanece no sufixo em todas as formas não preverbadas. Em algumas formas há um \textit{i} de ligação. Exemplo das três formas:

\ex. \textit{cakṛvā́ṃsas} nom.pl. x \textit{cakṛúṣas} acu.pl x \textit{cakṛvádbhis} instr.pl

\ex. \textit{cakṛvā́ṃsas} nom.pl. x \textit{cakṛúṣas} acu.pl x \textit{cakṛvádbhis} instr.pl

\subsubsection{-an, -van, -man}
\textit{\citegramnopar{420-38}{90-2}}

Não há formas específicas para o feminino. \textit{-van} é o mais comun, \textit{-an} o menos comum. A forma \textit{forte} é geralmente alongada com um \textit{ān}, a \textit{média} é apenas \textit{a} (vinda de uma resoante vocálica) e a forma \textit{fraca} varia entre \textit{an} e \textit{n} (nesse caso, sincopada).
A síncope é comum, mas nunca ocorre em temas em \textit{man} e \textit{van} quando são antecedidos por uma consoante. O \textit{n} cai com frequência no nominativo. Especificamente nessa declinação, é frequente a forma de locativo sem a desinência, ex.: \textit{mūrdhán} ''na cabeça'' e o nominativo-acusativo plural neutro pode ser expresso como \textit{kárma, kármā} ou \textit{kármāṇi} \citegram{425d}{90}. Exemplos: \textit{rā́jan} ''rei'', \textit{áśman} ''pedra'', \textit{ádhvan} ''caminho''. Há muitas excessões e particularidades, recomendo visitar os parágrafos sobre o assunto em ambos os gramáticos.

\subsubsection{-añc}
\textit{\citegramnopar{407-10}{93}}

Nem sempre palavras desse tipo formam três gradações, mas basicamente: o grau \textit{forte} é \textit{añc}, o grau \textit{médio} é \textit{ac} e o grau \textit{fraco} é \textit{c} eventualmente com alongamento da vogal anterior. As palavras costumam ser adjetivos formados por preposições. Exemplos: \textit{pratiyáñc} ''direcionado a'', \textit{samyáñc} ''unido'', \textit{udáñc} ''direcionado para cima''.

\ex.	\a. \textit{nyañc}, \textit{nyák}, \textit{nīc}
	\b. \textit{anváñc}, \textit{anvák}, \textit{anūc}
	\c. etc...

\pagebreak

\nocite{Macdonell1993,Whitney1889,Grassman1873,MonierWilliams2011}
\nocite{Kummel2000, Heenen2006}
\selectlanguage{english}
\printbibliography
\end{document}
